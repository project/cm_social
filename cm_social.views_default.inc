<?php
/**
 * @file
 * cm_social.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cm_social_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'social_media_icons';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Social media icons';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Channel icons';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = 'channel-icons';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Taxonomy term: Social media icon */
  $handler->display->display_options['fields']['field_sm_icon']['id'] = 'field_sm_icon';
  $handler->display->display_options['fields']['field_sm_icon']['table'] = 'field_data_field_sm_icon';
  $handler->display->display_options['fields']['field_sm_icon']['field'] = 'field_sm_icon';
  $handler->display->display_options['fields']['field_sm_icon']['label'] = '';
  $handler->display->display_options['fields']['field_sm_icon']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_sm_icon']['element_type'] = '0';
  $handler->display->display_options['fields']['field_sm_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sm_icon']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_sm_icon']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_sm_icon']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_sm_icon']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: URL */
  $handler->display->display_options['fields']['field_sm_url']['id'] = 'field_sm_url';
  $handler->display->display_options['fields']['field_sm_url']['table'] = 'field_data_field_sm_url';
  $handler->display->display_options['fields']['field_sm_url']['field'] = 'field_sm_url';
  $handler->display->display_options['fields']['field_sm_url']['label'] = '';
  $handler->display->display_options['fields']['field_sm_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_sm_url']['element_type'] = '0';
  $handler->display->display_options['fields']['field_sm_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_sm_url']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_sm_url']['element_default_classes'] = FALSE;
  /* Field: Rewrite with link */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Rewrite with link';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<a href="[field_sm_url]">[field_sm_icon]</a>';
  $handler->display->display_options['fields']['nothing_1']['element_type'] = '0';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing_1']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['nothing_1']['element_default_classes'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'social_media' => 'social_media',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['block_description'] = 'Social Media Icons';
  $translatables['social_media_icons'] = array(
    t('Master'),
    t('Channel icons'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<a href="[field_sm_url]">[field_sm_icon]</a>'),
    t('Block'),
    t('Social Media Icons'),
  );
  $export['social_media_icons'] = $view;

  return $export;
}
