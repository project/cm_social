<?php
/**
 * @file
 * cm_social.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cm_social_views_api() {
  return array("version" => "3.0");
}
